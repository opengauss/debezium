From e02a92ea6bd0d02fb113aa620bb0e9035ca9031d Mon Sep 17 00:00:00 2001
From: KeKe <wangxingmiao@huawei.com>
Date: Mon, 3 Jun 2024 16:36:02 +0800
Subject: [PATCH] parallel parse binlog events & flow control & mariadb adapt

---
 pom.xml                                       |   2 +-
 .../shyiko/mysql/binlog/BinaryLogClient.java  | 318 +++++++++++++++++-
 2 files changed, 318 insertions(+), 2 deletions(-)

diff --git a/pom.xml b/pom.xml
index 5920031..bccbeec 100644
--- a/pom.xml
+++ b/pom.xml
@@ -4,7 +4,7 @@
 
     <groupId>com.zendesk</groupId>
     <artifactId>mysql-binlog-connector-java</artifactId>
-    <version>0.25.4</version>
+    <version>0.25.4-modified</version>
 
     <name>mysql-binlog-connector-java</name>
     <description>MySQL Binary Log connector</description>
diff --git a/src/main/java/com/github/shyiko/mysql/binlog/BinaryLogClient.java b/src/main/java/com/github/shyiko/mysql/binlog/BinaryLogClient.java
index d8b8299..31a93b4 100644
--- a/src/main/java/com/github/shyiko/mysql/binlog/BinaryLogClient.java
+++ b/src/main/java/com/github/shyiko/mysql/binlog/BinaryLogClient.java
@@ -15,6 +15,7 @@
  */
 package com.github.shyiko.mysql.binlog;
 
+import com.github.shyiko.mysql.binlog.event.DeleteRowsEventData;
 import com.github.shyiko.mysql.binlog.event.Event;
 import com.github.shyiko.mysql.binlog.event.EventHeader;
 import com.github.shyiko.mysql.binlog.event.EventHeaderV4;
@@ -22,12 +23,15 @@ import com.github.shyiko.mysql.binlog.event.EventType;
 import com.github.shyiko.mysql.binlog.event.GtidEventData;
 import com.github.shyiko.mysql.binlog.event.QueryEventData;
 import com.github.shyiko.mysql.binlog.event.RotateEventData;
+import com.github.shyiko.mysql.binlog.event.UpdateRowsEventData;
+import com.github.shyiko.mysql.binlog.event.WriteRowsEventData;
 import com.github.shyiko.mysql.binlog.event.deserialization.ChecksumType;
 import com.github.shyiko.mysql.binlog.event.deserialization.EventDataDeserializationException;
 import com.github.shyiko.mysql.binlog.event.deserialization.EventDataDeserializer;
 import com.github.shyiko.mysql.binlog.event.deserialization.EventDeserializer;
 import com.github.shyiko.mysql.binlog.event.deserialization.EventDeserializer.EventDataWrapper;
 import com.github.shyiko.mysql.binlog.event.deserialization.GtidEventDataDeserializer;
+import com.github.shyiko.mysql.binlog.event.deserialization.MissingTableMapEventException;
 import com.github.shyiko.mysql.binlog.event.deserialization.QueryEventDataDeserializer;
 import com.github.shyiko.mysql.binlog.event.deserialization.RotateEventDataDeserializer;
 import com.github.shyiko.mysql.binlog.io.ByteArrayInputStream;
@@ -67,11 +71,17 @@ import java.net.SocketException;
 import java.security.GeneralSecurityException;
 import java.security.cert.CertificateException;
 import java.security.cert.X509Certificate;
+import java.time.LocalDateTime;
+import java.time.format.DateTimeFormatter;
+import java.util.ArrayList;
 import java.util.Arrays;
 import java.util.Collections;
 import java.util.LinkedList;
 import java.util.List;
+import java.util.Timer;
+import java.util.TimerTask;
 import java.util.concurrent.Callable;
+import java.util.concurrent.ConcurrentLinkedQueue;
 import java.util.concurrent.CopyOnWriteArrayList;
 import java.util.concurrent.CountDownLatch;
 import java.util.concurrent.ExecutorService;
@@ -79,6 +89,7 @@ import java.util.concurrent.Executors;
 import java.util.concurrent.ThreadFactory;
 import java.util.concurrent.TimeUnit;
 import java.util.concurrent.TimeoutException;
+import java.util.concurrent.atomic.AtomicBoolean;
 import java.util.concurrent.atomic.AtomicReference;
 import java.util.concurrent.locks.Lock;
 import java.util.concurrent.locks.ReentrantLock;
@@ -169,6 +180,21 @@ public class BinaryLogClient implements BinaryLogClientMXBean {
     private final Lock connectLock = new ReentrantLock();
     private final Lock keepAliveThreadExecutorLock = new ReentrantLock();
 
+    private int queryEventCount = 0;
+    private int dmlParseCount;
+    private int ddlParseCount;
+    private final int THREAD_NUM = 10;
+    private boolean isParallelParseEvent = false;
+    private boolean isProvideTransactionMetadata = true;
+    private volatile AtomicBoolean isBlock = new AtomicBoolean(false);
+    private int maxQueueSize;
+    private double openFlowControlThreshold;
+    private double closeFlowControlThreshold;
+
+    private List<ConcurrentLinkedQueue<byte[]>> packetQueueList = new ArrayList<>();
+    private List<ConcurrentLinkedQueue<Event>> eventQueueList = new ArrayList<>();
+    private final DateTimeFormatter ofPattern = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
+
     /**
      * Alias for BinaryLogClient("localhost", 3306, &lt;no schema&gt; = null, username, password).
      * @see BinaryLogClient#BinaryLogClient(String, int, String, String, String)
@@ -320,6 +346,14 @@ public class BinaryLogClient implements BinaryLogClientMXBean {
         }
     }
 
+    public int getDmlParseCount() {
+        return dmlParseCount;
+    }
+
+    public int getDdlParseCount() {
+        return ddlParseCount;
+    }
+
     /**
      * @param gtidSet GTID set (can be an empty string).
      * <p>NOTE #1: Any value but null will switch BinaryLogClient into a GTID mode (this will also set binlogFilename
@@ -501,6 +535,14 @@ public class BinaryLogClient implements BinaryLogClientMXBean {
         this.threadFactory = threadFactory;
     }
 
+    public void setIsParallelParseEvent(boolean isParallelParseEvent) {
+        this.isParallelParseEvent = isParallelParseEvent;
+    }
+
+    public void setIsProvideTransactionMetadata(boolean isProvideTransactionMetadata) {
+        this.isProvideTransactionMetadata = isProvideTransactionMetadata;
+    }
+
     /**
      * Connect to the replication stream. Note that this method blocks until disconnected.
      * @throws AuthenticationException if authentication fails
@@ -596,7 +638,11 @@ public class BinaryLogClient implements BinaryLogClientMXBean {
                     ensureEventDataDeserializer(EventType.QUERY, QueryEventDataDeserializer.class);
                 }
             }
-            listenForEventPackets();
+            if (isParallelParseEvent) {
+                listenForEventPacketsModified();
+            } else {
+                listenForEventPackets();
+            }
         } finally {
             connectLock.unlock();
             if (notifyWhenDisconnected) {
@@ -991,6 +1037,276 @@ public class BinaryLogClient implements BinaryLogClientMXBean {
         }
     }
 
+    private void listenForEventPacketsModified() throws IOException {
+        ByteArrayInputStream inputStream = channel.getInputStream();
+        boolean completeShutdown = false;
+        initialPacketEventQueue();
+        monitorEventQueueSize();
+        packet2Event();
+        mergeEvent();
+        statTask();
+        int packetLength = -1;
+        int marker = -1;
+        byte[] packet = null;
+        int index = 0;
+        try {
+            while (inputStream.peek() != -1) {
+                if (!isBlock.get()) {
+                    packetLength = inputStream.readInteger(3);
+                    inputStream.skip(1); // 1 byte for sequence
+                    marker = inputStream.read();
+                    if (marker == 0xFF) {
+                        ErrorPacket errorPacket = new ErrorPacket(inputStream.read(packetLength - 1));
+                        throw new ServerException(errorPacket.getErrorMessage(), errorPacket.getErrorCode(),
+                            errorPacket.getSqlState());
+                    }
+                    if (marker == 0xFE && !blocking) {
+                        completeShutdown = true;
+                        break;
+                    }
+                    try {
+                        if (packetLength == MAX_PACKET_LENGTH) {
+                            packet = readPacketSplitInChunks(inputStream, packetLength - 1);
+                            if (logger.isLoggable(Level.WARNING)) {
+                                logger.log(Level.WARNING, "[MAX_PACKET_LENGTH] receive a packet length "
+                                    + MAX_PACKET_LENGTH);
+                            }
+                        } else {
+                            packet = inputStream.read(packetLength - 1);
+                        }
+                        packetQueueList.get(index).add(packet);
+                        if (packet[4] == 16) {
+                            index++;
+                            if (index % THREAD_NUM == 0) {
+                                index = 0;
+                            }
+                        }
+                    } catch (Exception e) {
+                        Throwable cause = e instanceof EventDataDeserializationException ? e.getCause() : e;
+                        if (cause instanceof EOFException || cause instanceof SocketException) {
+                            throw e;
+                        }
+                        if (isConnected()) {
+                            for (LifecycleListener lifecycleListener : lifecycleListeners) {
+                                lifecycleListener.onEventDeserializationFailure(this, e);
+                            }
+                        }
+                        continue;
+                    }
+                } else {
+                    Thread.sleep(500);
+                }
+            }
+        } catch (Exception e) {
+            if (isConnected()) {
+                for (LifecycleListener lifecycleListener : lifecycleListeners) {
+                    lifecycleListener.onCommunicationFailure(this, e);
+                }
+            }
+        } finally {
+            if (isConnected()) {
+                if (completeShutdown) {
+                    disconnect(); // initiate complete shutdown sequence (which includes keep alive thread)
+                } else {
+                    disconnectChannel();
+                }
+            }
+        }
+    }
+
+    private void initialPacketEventQueue() {
+        for (int i = 0; i < THREAD_NUM; i++) {
+            packetQueueList.add(new ConcurrentLinkedQueue<>());
+            eventQueueList.add(new ConcurrentLinkedQueue<>());
+        }
+    }
+
+    private void packet2Event() {
+        for (int index = 0; index < THREAD_NUM; index++) {
+            startParseEventThread(index);
+        }
+    }
+
+    private void startParseEventThread(int index) {
+        new Thread(() -> {
+            try {
+                parseEvent(index);
+            } catch (IOException | InterruptedException exp) {
+                if (logger.isLoggable(Level.WARNING)) {
+                    logger.log(Level.WARNING, "Exception occurred", exp);
+                }
+            }
+        }).start();
+    }
+
+    private void parseEvent(int index) throws IOException, InterruptedException {
+        Thread.currentThread().setName("packet-to-event-" + index);
+        byte[] packet;
+        Event event = null;
+        while (true) {
+            packet = packetQueueList.get(index).poll();
+            if (packet != null) {
+                try {
+                    event = eventDeserializer.nextEvent(new ByteArrayInputStream(packet));
+                } catch (EventDataDeserializationException | MissingTableMapEventException exp) {
+                    if (logger.isLoggable(Level.WARNING)) {
+                        logger.log(Level.WARNING, "[Deserialization exception]" + exp.getMessage());
+                    }
+                } catch (IOException exp) {
+                    if (logger.isLoggable(Level.WARNING)) {
+                        logger.log(Level.WARNING, "[IO exception]" + exp.getMessage());
+                    }
+                }
+                if (event == null) {
+                    if (logger.isLoggable(Level.WARNING)) {
+                        logger.log(Level.WARNING, "Deserialize a null event");
+                    }
+                    break;
+                }
+                eventQueueList.get(index).add(event);
+            } else {
+                Thread.sleep(1);
+            }
+        }
+    }
+
+    private void mergeEvent() {
+        new Thread(() -> {
+            Thread.currentThread().setName("merge-event-thread");
+            Event event = null;
+            int index = 0;
+            int xidEventCount = 0;
+            int dmlCount = 0;
+            while (true) {
+                event = eventQueueList.get(index).poll();
+                if (event != null) {
+                    if (event.getHeader().getEventType() == EventType.QUERY) {
+                        queryEventCount++;
+                    }
+                    dmlCount += statDmlCount(event);
+                    if (event.getHeader().getEventType() == EventType.XID) {
+                        xidEventCount++;
+                        index++;
+                        if (index % THREAD_NUM == 0) {
+                            index = 0;
+                        }
+                    }
+                    ddlParseCount = queryEventCount - xidEventCount;
+                    dmlParseCount = dmlCount;
+                    if (connected) {
+                        eventLastSeen = System.currentTimeMillis();
+                        updateGtidSet(event);
+                        notifyEventListeners(event);
+                        updateClientBinlogFilenameAndPosition(event);
+                    }
+                } else {
+                    try {
+                        Thread.sleep(1000);
+                    } catch (InterruptedException e) {
+                        e.printStackTrace();
+                    }
+                }
+            }
+        }).start();
+    }
+
+    public int statDmlCount(Event event) {
+        switch (event.getHeader().getEventType()) {
+            // For MySQL 5.7: EXT_XXX_ROWS;
+            // For Mariadb 10.4.32: XXX_ROWS.
+            case EXT_WRITE_ROWS:
+            case WRITE_ROWS:
+                return ((WriteRowsEventData) event.getData()).getRows().size();
+            case EXT_UPDATE_ROWS:
+            case UPDATE_ROWS:
+                return ((UpdateRowsEventData) event.getData()).getRows().size();
+            case EXT_DELETE_ROWS:
+            case DELETE_ROWS:
+                return ((DeleteRowsEventData) event.getData()).getRows().size();
+            default:
+                return 0;
+        }
+    }
+
+    private void statTask() {
+        Timer timer = new Timer();
+        if (isProvideTransactionMetadata) {
+            final int[] before = {queryEventCount};
+            TimerTask task = new TimerTask() {
+                @Override
+                public void run() {
+                    String date = ofPattern.format(LocalDateTime.now());
+                    String result = String.format("have parsed %s QueryEvent, and current time is %s, and current "
+                        + "speed is %s", queryEventCount, date, queryEventCount - before[0]);
+                    if (logger.isLoggable(Level.WARNING)) {
+                        logger.log(Level.WARNING, result);
+                    }
+                    before[0] = queryEventCount;
+                }
+            };
+            timer.schedule(task, 1000, 1000);
+        } else {
+            final int[] before = {dmlParseCount + ddlParseCount};
+            TimerTask task = new TimerTask() {
+                @Override
+                public void run() {
+                    String date = ofPattern.format(LocalDateTime.now());
+                    String result = String.format("have parsed %s I/U/D/DDL events, and current time is %s, and current "
+                        + "speed is %s", dmlParseCount + ddlParseCount, date, dmlParseCount + ddlParseCount - before[0]);
+                    if (logger.isLoggable(Level.WARNING)) {
+                        logger.log(Level.WARNING, result);
+                    }
+                    before[0] = dmlParseCount + ddlParseCount;
+                }
+            };
+            timer.schedule(task, 1000, 1000);
+        }
+    }
+
+    public void initFlowControl(int maxQueueSize, double openFlowControlThreshold, double closeFlowControlThreshold) {
+        this.maxQueueSize = maxQueueSize;
+        this.openFlowControlThreshold = openFlowControlThreshold;
+        this.closeFlowControlThreshold = closeFlowControlThreshold;
+    }
+
+    private void monitorEventQueueSize() {
+        int openFlowControlQueueSize = (int) (openFlowControlThreshold * maxQueueSize);
+        int closeFlowControlQueueSize = (int) (closeFlowControlThreshold * maxQueueSize);
+        TimerTask task = new TimerTask() {
+            @Override
+            public void run() {
+                Thread.currentThread().setName("timer-queue-size");
+                for (int i = 0; i < THREAD_NUM; i++) {
+                    int size = eventQueueList.get(i).size();
+                    if (size > openFlowControlQueueSize) {
+                        if (!isBlock.get()) {
+                            logger.warning("[start flow control] current isBlock is " + isBlock + ", queue size is " + size + ", which is "
+                                + "more than " + openFlowControlThreshold + " * " + maxQueueSize + ", so open flow control");
+                        }
+                        isBlock.set(true);
+                        break;
+                    }
+                }
+                int num = 0;
+                for (int i = 0; i < THREAD_NUM; i++) {
+                    int size = eventQueueList.get(i).size();
+                    if (size < closeFlowControlQueueSize) {
+                        num++;
+                    }
+                }
+                if (num == THREAD_NUM) {
+                    if (isBlock.get()) {
+                        logger.warning("[close flow control] current isBlock is " + isBlock + ", all eventQueue size is lower than "
+                            + closeFlowControlThreshold + " * " + maxQueueSize + ", so close flow control.");
+                    }
+                    isBlock.set(false);
+                }
+            }
+        };
+        Timer timer = new Timer();
+        timer.schedule(task, 10, 20);
+    }
+
     private byte[] readPacketSplitInChunks(ByteArrayInputStream inputStream, int packetLength) throws IOException {
         byte[] result = inputStream.read(packetLength);
         int chunkLength;
-- 
2.44.0.windows.1

